CREATE TABLE public.users
(
    id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    name character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    email_id character varying(100) COLLATE pg_catalog."default" NOT NULL DEFAULT NULL::character varying,
    password character varying(100) COLLATE pg_catalog."default" NOT NULL DEFAULT NULL::character varying,
    mobile bigint NOT NULL,
    role_id integer,
    is_active boolean DEFAULT true,
    created_by integer,
    created_at timestamp(6) with time zone DEFAULT now(),
    updated_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)

ALTER TABLE public.users
    OWNER to postgres;


CREATE TABLE public.products
(
    id serial4 PRIMARY KEY ,
    name character varying(255) NOT NULL,
	stock integer NOT NULL DEFAULT 0,
	price float4 NOT NULL,
	shortDesc character varying(255),
	description character varying(255),
    is_active boolean DEFAULT true,
    created_by integer,
    created_at timestamp(6) with time zone DEFAULT now(),
    updated_at timestamp(6) with time zone DEFAULT NULL::timestamp with time zone
    
)

ALTER TABLE public.products
    OWNER to postgres;


CREATE TABLE "public"."carts" (
  "id" serial4 PRIMARY KEY ,  
  "product_id" int4 NOT NULL DEFAULT NULL,
  "user_id" int4 NOT NULL DEFAULT NULL,
  "qty" integer NOT NULL,
  "is_active" bool DEFAULT true,
  "created_by" int4 DEFAULT NULL,
  "created_at" timestamptz(6) DEFAULT now(),
  "updated_at" timestamptz(6) DEFAULT NULL  
)
;


-- ----------------------------
-- Foreign Keys structure for table examination_questions
-- ----------------------------
ALTER TABLE "public"."carts" ADD CONSTRAINT "product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "products" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."carts" ADD CONSTRAINT "user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;



CREATE TABLE "public"."orders" (
  "order_id" serial4 PRIMARY KEY ,  
  "product_id" int4 NOT NULL DEFAULT NULL,
  "user_id" int4 NOT NULL DEFAULT NULL,
  "qty" integer NOT NULL,
  "is_active" bool DEFAULT true,
  "created_by" int4 DEFAULT NULL,
  "created_at" timestamptz(6) DEFAULT now(),
  "updated_at" timestamptz(6) DEFAULT NULL  
)
;


-- ----------------------------
-- Foreign Keys structure for table examination_questions
-- ----------------------------
ALTER TABLE "public"."orders" ADD CONSTRAINT "product_id_fkey" FOREIGN KEY ("product_id") REFERENCES "products" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."orders" ADD CONSTRAINT "user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

