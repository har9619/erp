const { Pool } = require('pg')

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'erpassignment',
  password: 'admin123',
  post: 5432
})

module.exports = pool
